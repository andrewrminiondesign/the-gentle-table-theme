<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The_Gentle_Table
 */

?>

    </div><!-- #content -->

    <footer id="colophon" class="site-footer">
        <div class="site-info">
            Powered by <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'the-gentle-table' ) ); ?>">WordPress</a>
            <span class="sep"> | </span>
            Custom theme by <a href="https://andrewrminion.com/?ref=thegentletable.com">AndrewRMinion Design</a>
        </div><!-- .site-info -->
    </footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

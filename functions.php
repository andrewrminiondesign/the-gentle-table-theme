<?php

define( 'TGT_THEME_VERSION', wp_get_theme()->get( 'Version' ) );

/**
 * The Gentle Table functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package The_Gentle_Table
 */

if ( ! function_exists( 'the_gentle_table_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function the_gentle_table_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on The Gentle Table, use a find and replace
         * to change 'the-gentle-table' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'the-gentle-table', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'menu-1' => esc_html__( 'Primary', 'the-gentle-table' ),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        // Set up the WordPress core custom background feature.
        add_theme_support( 'custom-background', apply_filters( 'the_gentle_table_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        ) ) );

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support( 'custom-logo', array(
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        ) );
    }
endif;
add_action( 'after_setup_theme', 'the_gentle_table_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function the_gentle_table_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'the_gentle_table_content_width', 640 );
}
add_action( 'after_setup_theme', 'the_gentle_table_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function the_gentle_table_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'the-gentle-table' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'the-gentle-table' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'the_gentle_table_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function the_gentle_table_scripts() {
    wp_enqueue_style( 'the-gentle-table-style', get_stylesheet_directory_uri() . '/assets/css/style.min.css', array(), TGT_THEME_VERSION );
    wp_dequeue_style( 'peepso-custom' );
    wp_enqueue_style( 'peepso-custom', get_stylesheet_directory_uri() . '/peepso/custom.css', array(), TGT_THEME_VERSION, 'all' );

    wp_enqueue_script( 'the-gentle-table-navigation', get_template_directory_uri() . '/assets/js/navigation.min.js', array(), TGT_THEME_VERSION, true );
    wp_enqueue_script( 'the-gentle-table-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.min.js', array(), TGT_THEME_VERSION, true );

    $webfonts = "
    WebFont.load({
        google: {
          families: ['Merriweather+Sans:400,400i,700,700i','Merriweather:700,700i']
        },
    });";

    wp_enqueue_script( 'webfont-loader', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js' );
    wp_add_inline_script( 'webfont-loader', $webfonts );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'the_gentle_table_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
    require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Add new directory for Peepso templates
 */
if ( class_exists( 'PeepSoTemplate' ) ) {
  $peepso_template = new PeepSoTemplate();
  $peepso_template->add_template_directory( get_stylesheet_directory() . '/peepso/' );
}

